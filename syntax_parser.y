%union {
int t_int;
double t_double;
char t_string[256];
}

%{
    	#include <stdio.h>
	#include <string.h>

	extern int dxf_lex(YYSTYPE *);
	int syntax_lex(YYSTYPE *);
	int yyerror(const char *);
	int codeCheck(int);
	void resetCodes();
	int sectionCheck (const char *);
	int myError(const char *, int);
	int arcCodes();
	int cirCodes();
	int linCodes();
	int polCodes();
	int verCodes();
	
	unsigned char codes[1071];
	unsigned char sections[7];
%}

%pure-parser
%defines
%name-prefix="syntax_"
%error-verbose

%token eof endsection seqend missmatchtype
%token <t_int> linepair
%token <t_double> xvalue yvalue radius startangle endangle xend yend
%token <t_string> section sectiontype blocks entities entitytype arc circle line polyline vertex

%%

START		: DXF
		;

DXF		: eof { return 0; }
		| section SECTIONTYPE
		;
			
SECTIONTYPE	: sectiontype { if(!sectionCheck($1)) return myError("syntax error, repeated section!",1); } SECTIONBODY
		| blocks { if(!sectionCheck($1)) return myError("syntax error, repeated section!",1); if(sections[4]>=1) return myError("syntax error, 'BLOCKS' after 'ENTITIES'",1); } BLOCKSBODY
		| entities { if(!sectionCheck($1)) return myError("syntax error, repeated section!",1); } ENTITIESBODY
		;

SECTIONBODY	: endsection DXF
		| linepair SECTIONBODY
		;
			
BLOCKSBODY	: endsection section entities { if(!sectionCheck($3)) return myError("syntax error, repeated section!",1); } ENTITIESBODY
		| linepair BLOCKSBODY
		;
			
ENTITIESBODY	: endsection DXF
		| entitytype ENTITIESCODES
		| arc ARCCODES
		| circle CIRCLECODES
		| line LINECODES
		| polyline POLYLINECODES
		;
			
ENTITIESCODES	: { resetCodes(); } ENTITIESBODY
		| linepair { if(!codeCheck($1)) return myError("syntax error, repeated code!",2); } ENTITIESCODES
		;
			
ARCCODES	: { arcCodes(); } ENTITIESBODY
		| linepair { if(!codeCheck($1)) return myError("syntax error, repeated code!",2); } ARCCODES
		| xvalue { if(!codeCheck(10)) return myError("syntax error, repeated code!",2); } ARCCODES
		| yvalue { if(!codeCheck(20)) return myError("syntax error, repeated code!",2); } ARCCODES
		| radius { if(!codeCheck(40)) return myError("syntax error, repeated code!",2); } ARCCODES
		| startangle { if(!codeCheck(50)) return myError("syntax error, repeated code!",2); } ARCCODES
		| endangle { if(!codeCheck(51)) return myError("syntax error, repeated code!",2); } ARCCODES
		;
			
CIRCLECODES	: { cirCodes(); }ENTITIESBODY
		| linepair { if(!codeCheck($1)) return myError("syntax error, repeated code!",2); } CIRCLECODES
		| xvalue { if(!codeCheck(10)) return myError("syntax error, repeated code!",2); } CIRCLECODES
		| yvalue { if(!codeCheck(20)) return myError("syntax error, repeated code!",2); } CIRCLECODES
		| radius { if(!codeCheck(40)) return myError("syntax error, repeated code!",2); } CIRCLECODES
		;
			
LINECODES	: { linCodes(); } ENTITIESBODY
		| linepair { if(!codeCheck($1)) return myError("syntax error, repeated code!",2); } LINECODES
		| xvalue { if(!codeCheck(10)) return myError("syntax error, repeated code!",2); } LINECODES
		| yvalue { if(!codeCheck(20)) return myError("syntax error, repeated code!",2); } LINECODES
		| xend { if(!codeCheck(11)) return myError("syntax error, repeated code!",2); } LINECODES
		| yend { if(!codeCheck(21)) return myError("syntax error, repeated code!",2); } LINECODES
		;
			
POLYLINECODES	: { polCodes(); } VERTEXS
		| linepair { if(!codeCheck($1)) return myError("syntax error, repeated code!",2); } POLYLINECODES
		| xvalue { if ($1 != 0) return myError ("syntax error, xvalue is not 0",1); if(!codeCheck(10)) return myError("syntax error, repeated code!",2); } POLYLINECODES
		| yvalue { if ($1 != 0) return myError ("syntax error, yvalue is not 0",1); if(!codeCheck(20)) return myError("syntax error, repeated code!",2); } POLYLINECODES
		;
			
VERTEXS		: seqend SEQENDCODES
		| vertex VERTEXCODES
		;
			
SEQENDCODES	: { resetCodes(); } ENTITIESBODY
		| linepair { if(!codeCheck($1)) return myError("syntax error, repeated code!",2); } SEQENDCODES
		;
			
VERTEXCODES	: { verCodes(); } VERTEXS
		| linepair { if(!codeCheck($1)) return myError("syntax error, repeated code!",2); } VERTEXCODES
		| xvalue { if(!codeCheck(10)) return myError("syntax error, repeated code!",2); } VERTEXCODES
		| yvalue { if(!codeCheck(20)) return myError("syntax error, repeated code!",2); } VERTEXCODES
		;
			
%%

int syntax_lex(YYSTYPE *dxf_value)
{
	return dxf_lex(dxf_value);
}

int yyerror(const char *s)
{
    	extern int dxf_lineno;
    	printf("%s @ line %d\n", s, dxf_lineno);
    	return 1;
}

int sectionCheck (const char *x)
{
	if(!strcmp(x,"HEADER"))		sections[0]++;
	if(!strcmp(x,"CLASSES"))	sections[1]++;
	if(!strcmp(x,"TABLES"))		sections[2]++;
	if(!strcmp(x,"BLOCKS"))		sections[3]++;
	if(!strcmp(x,"ENTITIES"))	sections[4]++;
	if(!strcmp(x,"OBJECTS"))	sections[5]++;
	if(!strcmp(x,"THUMBNAILIMAGE"))	sections[6]++;

	int i;
	for(i=0; i<7; i++)
		if(sections[i]>1)
			return 0;
	return 1;
}

void resetCodes()
{
	int i;
	for(i=0; i<1071; i++)
		codes[i]=0;
}

int codeCheck(int code)
{
	if(++codes[code]>1)
		return 0;
	return 1;
}

int myError(const char *x, int nlines)
{
    	extern int dxf_lineno;
    	printf("%s @ line %d\n", x, dxf_lineno-nlines);
    	return 1;
}

int arcCodes()
{
	if (codes[10] < 1) return 0;
	if (codes[20] < 1) return 0;
	if (codes[40] < 1) return 0;
	if (codes[50] < 1) return 0;
	if (codes[51] < 1) return 0;
	resetCodes();
	return 1;
}

int cirCodes()
{
	if (codes[10] < 1) return 0;
	if (codes[20] < 1) return 0;
	if (codes[40] < 1) return 0;
	resetCodes();
	return 1;
}

int linCodes()
{
	if (codes[10] < 1) return 0;
	if (codes[11] < 1) return 0;
	if (codes[20] < 1) return 0;
	if (codes[21] < 1) return 0;
	resetCodes();
	return 1;
}

int polCodes()
{
	if (codes[10] < 1) return 0;
	if (codes[20] < 1) return 0;
	resetCodes();
	return 1;
}

int verCodes()
{
	if (codes[10] < 1) return 0;
	if (codes[20] < 1) return 0;
	resetCodes();
	return 1;
}
