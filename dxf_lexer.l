%{
   	#include "syntax_parser.h"
	#include "statistics_parser.h"
	#include "svg_parser.h"
    	#define YY_DECL int dxf_lex(YYSTYPE* dxf_value)
%}

%option prefix="dxf_"

%option noyywrap
%option nounput
%option noinput

%option yylineno

blk		[ \t]*
bwn		{blk}"\n"
bnb		{bwn}{blk}

dig		[0-9]
itg		{dig}+
dbl		"-"?({itg}|{itg}"."{dig}*|{dig}*"."{itg})(("e"|"E")("+"|"-")?{itg})?
str		[^\n]*

eof			{blk}"0"{bnb}"EOF"{bwn}
section			{blk}"0"{bnb}"SECTION"{bwn}
sectiontype 		{blk}"2"{bnb}("HEADER"|"CLASSES"|"TABLES"|"OBJECTS"|"THUMBNAILIMAGE"){bwn}
blocks			{blk}"2"{bnb}"BLOCKS"{bwn}
endsection		{blk}"0"{bnb}"ENDSEC"{bwn}
entities		{blk}"2"{bnb}"ENTITIES"{bwn}
entitytype		{blk}"0"{bnb}("3DFACE"|"3DSOLID"|"ACAD_PROXY_ENTITY"|"ATTDEF"|"ATTRIB"|"BODY"|"DIMENSION"|"ELLIPSE"|"HATCH"|"HELIX"|"IMAGE"|"INSERT"|"LEADER"|"LIGHT"|"LWPOLYLINE"|"MESH"|"MLINE"|"MLEADERSTYLE"|"MLEADER"|"MTEXT"|"OLEFRAME"|"OLE2FRAME"|"POINT"|"RAY"|"REGION"|"SECTION"|"SHAPE"|"SOLID"|"SPLINE"|"SUN"|"SURFACE"|"TABLE"|"TEXT"|"TOLERANCE"|"TRACE"|"UNDERLAY"|"VIEWPORT"|"WIPEOUT"|"XLINE"){bwn}
arc			{blk}"0"{bnb}"ARC"{bwn}
circle			{blk}"0"{bnb}"CIRCLE"{bwn}
line			{blk}"0"{bnb}"LINE"{bwn}
polyline		{blk}"0"{bnb}"POLYLINE"{bwn}
xvalue			{blk}"10"{bnb}{dbl}{bwn}
yvalue			{blk}"20"{bnb}{dbl}{bwn}
radius			{blk}"40"{bnb}{dbl}{bwn}
startangle		{blk}"50"{bnb}{dbl}{bwn}
endangle		{blk}"51"{bnb}{dbl}{bwn}
xend			{blk}"11"{bnb}{dbl}{bwn}
yend			{blk}"21"{bnb}{dbl}{bwn}
seqend			{blk}"0"{bnb}"SEQEND"{bwn}
vertex			{blk}"0"{bnb}"VERTEX"{bwn}
cmt			{blk}"999"{bnb}{str}{bwn}

codebol			{blk}"29"{dig}{bnb}[01]{bwn}
codeint			{blk}([679]{dig}|"1"[67]{dig}|[23][78]{dig}|"4"[0245]{dig}|"106"{dig}|"107"[01]){bnb}{itg}{bwn}
codedbl			{blk}("10"?[1-5]{dig}|"1"[1-4]{dig}|"2"[1-3]{dig}|"46"{dig}){bnb}{dbl}{bwn}
codestr			{blk}({dig}|"10"[025]|"3"[0-69]{dig}|"4"[1378]{dig}|"999"|"100"{dig}){bnb}{str}{bwn}

%%

{eof}			{ return eof; }

{endsection} 		{ return endsection; }

{seqend}		{ return seqend; }

{section}		{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return section; }
{sectiontype}		{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return sectiontype; }
{blocks}		{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return blocks; }
{entities}		{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return entities; }
{entitytype}		{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return entitytype; }
{arc}			{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return arc; }
{circle}		{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return circle; }
{line}			{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return line; }
{polyline}		{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return polyline; }
{vertex}		{ sscanf (dxf_text,"%*d\n%s\n",dxf_value->t_string); return vertex; }

{xvalue}		{ sscanf (dxf_text,"%*d\n%lg\n",&(dxf_value->t_double)); return xvalue; }
{yvalue}		{ sscanf (dxf_text,"%*d\n%lg\n",&(dxf_value->t_double)); return yvalue; }
{radius}		{ sscanf (dxf_text,"%*d\n%lg\n",&(dxf_value->t_double)); return radius; }
{startangle}		{ sscanf (dxf_text,"%*d\n%lg\n",&(dxf_value->t_double)); return startangle; }
{endangle}		{ sscanf (dxf_text,"%*d\n%lg\n",&(dxf_value->t_double)); return endangle; }
{xend}			{ sscanf (dxf_text,"%*d\n%lg\n",&(dxf_value->t_double)); return xend; }
{yend}			{ sscanf (dxf_text,"%*d\n%lg\n",&(dxf_value->t_double)); return yend; }

{cmt}|\n		{ }

{codestr}|{codeint}|{codedbl}|{codebol}		{ sscanf (dxf_text,"%d\n%*s\n",&(dxf_value->t_int)); return linepair; }

.			{ return missmatchtype; }

%%
