CC = g++
CFLAGS = -Wall

CLASSES = Classes_DOT/Arco Classes_DOT/Circulo Classes_DOT/Line Classes_DOT/Poly Classes_DOT/Ponto Classes_DOT/Vertex
OBJECTS = Classes_DOT/Arco.o Classes_DOT/Circulo.o Classes_DOT/Line.o Classes_DOT/Poly.o Classes_DOT/Ponto.o Classes_DOT/Vertex.o

all:    $(ENT) $(CLASSES) dxf

dxf:  main-c.o Classes_DOT/Entity.o $(OBJECTS) syntax_parser.o statistics_parser.o svg_parser.o dot_parser.o dxf_lexer.o
	$(CC) -o $@ $^ -lm

Entity:	
	$(CC) $(CFLAGS) -o $@.o -c $@.cpp
	
syntax_parser.c: syntax_parser.y
	bison  -o $@ $<

syntax_parser.h: syntax_parser.y

statistics_parser.c: statistics_parser.y
	bison  -o $@ $<

statistics_parser.h: statistics_parser.y

svg_parser.c: svg_parser.y
	bison  -o $@ $<

svg_parser.h: svg_parser.y

dot_parser.c: dot_parser.y
	bison  -o $@ $<

dot_parser.h: dot_parser.y

dxf_lexer.c:	dxf_lexer.l
	flex -o$@ $<

$(CLASSES): 
	$(CC) $(CFLAGS) -o $@.o -c $@.cpp	
	
clean:
	rm -f dxf_lexer.c syntax_parser.[ch] statistics_parser.[ch] svg_parser.[ch] dot_parser.[ch] *.o *.dot *.html *.svg Classes_DOT/*.p

cleanall:   clean
	rm -f dxf
