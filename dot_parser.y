%union {
int t_int;
double t_double;
char t_string[256];
}

%{
	#include <stdio.h>
	#include <iostream>
	#include <sstream>
	#include <map>
	#include <set>
	#include <list>
	#include <string.h>
	#include "Classes_DOT/Ponto.h"
	#include "Classes_DOT/Arco.h"
	#include "Classes_DOT/Circulo.h"
	#include "Classes_DOT/Line.h"
	#include "Classes_DOT/Poly.h"
	#include "Classes_DOT/Vertex.h"
	#include "Classes_DOT/Entity.h"
	
	extern int dxf_lex(YYSTYPE *);
	int dot_lex(YYSTYPE* );
	int dot_error(const char*);
	int dot_make();
	list<Entity*> *findElement(Ponto&);
	
	extern FILE* dxf_out;
	extern FILE* dxf_in;
	Entity* p;
	map<Ponto, list<Entity* > > Mapa;
	set<Ponto> Set;
	
	using namespace std;	
%}
%pure-parser
%defines
%name-prefix="dot_"
%error-verbose

%token eof endsection seqend invalidexpression
%token <t_int> linepair
%token <t_double> xvalue yvalue radius startangle endangle xend yend
%token <t_string> section sectiontype blocks entities entitytype arc circle line polyline vertex

%%

START		: DXF
		;

DXF		: eof { dot_make(); }
		| section SECTIONTYPE
		;
			
SECTIONTYPE	: sectiontype SECTIONBODY
		| blocks BLOCKSBODY
		| entities ENTITIESBODY
		;

SECTIONBODY	: endsection DXF
		| linepair SECTIONBODY
		;
			
BLOCKSBODY	: endsection section entities ENTITIESBODY
		| linepair BLOCKSBODY
		;
			
ENTITIESBODY	: endsection DXF
		| entitytype ENTITIESCODES
		| arc { p = new Arco(); }ARCCODES 
		| circle { p = new Circulo(); } CIRCLECODES 
		| line { p = new Line(); }LINECODES
		| polyline { p = new Poly(); } POLYLINECODES
		;
			
ENTITIESCODES	: ENTITIESBODY
		| linepair ENTITIESCODES
		;
			
ARCCODES	: { ((Arco*)p)->calcExtra(); if(p->isValid()){ findElement(((Arco*)p)->extr1)->push_back(p); findElement(((Arco*)p)->extr2)->push_back(p); }
		} ENTITIESBODY
		| linepair ARCCODES
		| xvalue { ((Arco*)p)->centro.setX($1);	 ((Arco*)p)->flag |= 1; ((Arco*)p)->calcExtra(); } ARCCODES
		| yvalue { ((Arco*)p)->centro.setY($1); ((Arco*)p)->flag |= 1; ((Arco*)p)->calcExtra(); } ARCCODES
		| radius { ((Arco*)p)->setRaio($1); } ARCCODES
		| startangle{ ((Arco*)p)->setAng1($1); } ARCCODES
		| endangle { ((Arco*)p)->setAng2($1); }ARCCODES
		;
			
CIRCLECODES	: { if (p->isValid()) { findElement(((Circulo*)p)->centro)->push_back(p); }	} ENTITIESBODY
		| linepair CIRCLECODES
		| xvalue { ((Circulo*)p)->centro.setX($1); ((Circulo*)p)->flag |= 1; } CIRCLECODES
		| yvalue { ((Circulo*)p)->centro.setY($1); ((Circulo*)p)->flag |= 1; } CIRCLECODES
		| radius { ((Circulo*)p)->setRadius($1); } CIRCLECODES
		;
			
LINECODES	: {	if(p->isValid()) { findElement(((Line*)p)->x)->push_back(p); findElement(((Line*)p)->y)->push_back(p); } } ENTITIESBODY
		| linepair LINECODES
		| xvalue{ ((Line*)p)->x.setX($1); ((Line*)p)->flag |= 1; } LINECODES
		| yvalue{ ((Line*)p)->x.setY($1); ((Line*)p)->flag |= 1; } LINECODES
		| xend { ((Line*)p)->y.setX($1); ((Line*)p)->flag |= 2; }LINECODES
		| yend { ((Line*)p)->y.setY($1); ((Line*)p)->flag |= 2; }LINECODES
		;
			
POLYLINECODES	: VERTEXS
		| linepair POLYLINECODES
		| xvalue POLYLINECODES
		| yvalue POLYLINECODES
		;
			
VERTEXS		: seqend SEQENDCODES
		| vertex VERTEXCODES
		;
			
SEQENDCODES	:  { ((Poly*)p)->nVertex++; if (p->isValid()){ findElement(((Poly*)p)->vertices[((Poly*)p)->nVertex-1].x)->push_back(p); } } ENTITIESBODY
		| linepair SEQENDCODES
		;
			
VERTEXCODES	: VERTEXS
		| linepair VERTEXCODES
		| xvalue { ((Poly*)p)->vertices[((Poly*)p)->nVertex].x.setX($1); ((Poly*)p)->vertices[((Poly*)p)->nVertex].flag |= 1;} VERTEXCODES
		| yvalue { ((Poly*)p)->vertices[((Poly*)p)->nVertex].x.setY($1); ((Poly*)p)->vertices[((Poly*)p)->nVertex].flag |= 1;} VERTEXCODES
		;
			
%%

int dot_lex(YYSTYPE *dxf_value)
{
		return dxf_lex(dxf_value);
}

int yyerror(const char* s)
{
    	extern int dxf_lineno;
    	printf("%s @ line %d\n", s, dxf_lineno);
    	return 1;
}

list<Entity*> *findElement(Ponto &pot)
{
	map<Ponto,list<Entity*> >::iterator it;
	set<Ponto >::iterator sit;
	for(sit = Set.begin(); sit != Set.end(); sit++)
		if(pot.equals( *sit) )
			return &Mapa[*sit];
	Set.insert(pot);
	list<Entity*> l;
	Mapa[pot] = l;
	return &Mapa[pot];
}

int dot_make()
{
	set<Ponto>::iterator sit;
	list<Entity*>::iterator lit;
	fprintf(dxf_out, "graph {\n");
	for(sit = Set.begin(); sit != Set.end(); sit++)
	{
		if( !( ((Ponto)*sit).isValid() ) ) continue;
		fprintf(dxf_out, "\t");
		lit = Mapa[*sit].begin();
		while(lit != Mapa[*sit].end())
		{
			if(  !(((Entity*) *lit )->isValid()) ) continue;
			fprintf(dxf_out, "%s",&(((Entity*) *lit	)->print()[0]));
			if(++lit != Mapa[*sit].end()) fprintf(dxf_out, "--");	
		}
		fprintf(dxf_out, "\t\t // %s \n",&(((Ponto)*sit).print()[0]));
	}
	fprintf(dxf_out, "}\n");
	return 0;
}
