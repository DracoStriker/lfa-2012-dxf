%union {
int t_int;
double t_double;
char t_string[256];
}

%{
	
	#include <stdio.h>

	#define X_MAX 1000.0
	#define Y_MAX 1000.0
	
	extern int dxf_lex(YYSTYPE *);
	int svg_lex(YYSTYPE *);
	int yyerror(const char* s);
	void printsvgstart();
	void printsvgend();
	void printarc();
	void printline();
	void printcircle();
	void printvertex();
	void printpolylinestart();
	void printpolylineend();

   	extern FILE* dxf_out;
	double x, y, x_e, y_e, rad, s_angle, e_angle;
%}

%pure-parser
%defines
%name-prefix="svg_"
%error-verbose

%token eof endsection seqend missmatchtype
%token <t_int> linepair
%token <t_double> xvalue yvalue radius startangle endangle xend yend
%token <t_string> section sectiontype blocks entities entitytype arc circle line polyline vertex

%%

START		: { printsvgstart(); } DXF
		;

DXF		: eof { printsvgend(); return 0; }
		| section SECTIONTYPE
		;
			
SECTIONTYPE	: sectiontype SECTIONBODY
		| blocks BLOCKSBODY
		| entities ENTITIESBODY
		;

SECTIONBODY	: endsection DXF
		| linepair SECTIONBODY
		;
			
BLOCKSBODY	: endsection section entities ENTITIESBODY
		| linepair BLOCKSBODY
		;
			
ENTITIESBODY	: endsection DXF
		| entitytype ENTITIESCODES
		| arc ARCCODES
		| circle CIRCLECODES
		| line LINECODES
		| polyline { printpolylinestart(); } POLYLINECODES
		;
			
ENTITIESCODES	: ENTITIESBODY
		| linepair ENTITIESCODES
		;
			
ARCCODES	: { printarc(); } ENTITIESBODY
		| linepair ARCCODES
		| xvalue { x = $1; } ARCCODES
		| yvalue { y = $1; } ARCCODES
		| radius { rad = $1; } ARCCODES
		| startangle { s_angle = $1; } ARCCODES
		| endangle { e_angle = $1; } ARCCODES
		;
			
CIRCLECODES	: { printcircle(); } ENTITIESBODY
		| linepair CIRCLECODES
		| xvalue { x = $1; } CIRCLECODES
		| yvalue { y = $1; } CIRCLECODES
		| radius { rad = $1; } CIRCLECODES
		;
			
LINECODES	: { printline(); } ENTITIESBODY
		| linepair LINECODES
		| xvalue { x = $1; } LINECODES
		| yvalue { y = $1; } LINECODES
		| xend { x_e = $1; } LINECODES
		| yend { y_e = $1; } LINECODES
		;
			
POLYLINECODES	: VERTEXS
		| linepair POLYLINECODES
		| xvalue  POLYLINECODES
		| yvalue  POLYLINECODES
		;
			
VERTEXS		: seqend SEQENDCODES
		| vertex VERTEXCODES
		;
			
SEQENDCODES	: { printpolylineend(); } ENTITIESBODY
		| linepair SEQENDCODES
		;
			
VERTEXCODES	: { printvertex(); } VERTEXS
		| linepair VERTEXCODES
		| xvalue { x = $1; } VERTEXCODES
		| yvalue { y = $1; } VERTEXCODES
		;
			
%%

int svg_lex(YYSTYPE *dxf_value)
{
	return dxf_lex(dxf_value);
}

int yyerror(const char* s)
{
    	extern int dxf_lineno;
    	printf("%s @ line %d\n", s, dxf_lineno);
    	return 1;
}

void printsvgstart()
{
	fprintf(dxf_out,"<svg height=\"%lg\" width=\"%lg\">\n",X_MAX,Y_MAX);
}

void printsvgend()
{
	fprintf(dxf_out,"</svg>");
}

void printarc()
{
	fprintf(dxf_out,"<path d= \"M %lg %lg A %lg %lg 0 0 0 %lg %lg \" stroke=\"black\" fill=\"none\" />\n",x,y,rad,rad,s_angle,e_angle);
	x=y=rad=s_angle=e_angle=0;
}

void printline()
{
	fprintf(dxf_out,"<line x1=\"%lg\" y1=\"%lg\" x2=\"%lg\" y2=\"%lg\" style=\"stroke:rgb(0,0,0); stroke-width:1\" />\n",x,y,x_e,y_e);
	x=y=x_e=y_e=0;
}

void printcircle()
{
	fprintf(dxf_out,"<circle cx=\"%lg\" cy=\"%lg\" r=\"%lg\" stroke=\"black\" stroke-width:1\" fill=\"none\"/>\n",x,y,rad);
	x=y=rad=0;
}

void printvertex()
{
	fprintf(dxf_out,"%lg,%lg ",x,y);
	x=y=0;
}

void printpolylinestart()
{
	fprintf(dxf_out,"<polyline points = \"");
}

void printpolylineend()
{
	fprintf(dxf_out,"\" style = \"fill:none;stroke:black;stroke-width:1\" />\n");
}

