%union {
int t_int;
double t_double;
char t_string[256];
}

%{
	#include <stdio.h>

	extern int dxf_lex(YYSTYPE *);
	int statistics_lex(YYSTYPE *);
    	int yyerror(const char* s);
	void imprime();
	void initializeCodes();
	
	int arcc, polc, linc, verc, circ, verMax, verMin, ver;

%}

%pure-parser
%defines
%name-prefix="statistics_"
%error-verbose

%token eof endsection seqend missmatchtype
%token <t_int> linepair
%token <t_double> xvalue yvalue radius startangle endangle xend yend
%token <t_string> section sectiontype blocks entities entitytype arc circle line polyline vertex

%%

START		: { initializeCodes(); } DXF
		;

DXF		: eof { imprime(); return 0; }
		| section SECTIONTYPE
		;
			
SECTIONTYPE	: sectiontype { printf("%s existe!\n",$1); } SECTIONBODY
		| blocks { printf("%s existe!\n",$1); } BLOCKSBODY
		| entities ENTITIESBODY
		;

SECTIONBODY	: endsection DXF
		| linepair SECTIONBODY
		;
			
BLOCKSBODY	: endsection section entities ENTITIESBODY
		| linepair BLOCKSBODY
		;
			
ENTITIESBODY	: endsection DXF
		| entitytype ENTITIESCODES
		| arc { arcc++; } ARCCODES
		| circle { circ++; } CIRCLECODES
		| line { linc++; } LINECODES
		| polyline { polc++; } POLYLINECODES
		;
			
ENTITIESCODES	: ENTITIESBODY
		| linepair ENTITIESCODES
		;
			
ARCCODES	: ENTITIESBODY
		| linepair ARCCODES
		| xvalue ARCCODES
		| yvalue ARCCODES
		| radius ARCCODES
		| startangle ARCCODES
		| endangle ARCCODES
		;
			
CIRCLECODES	: ENTITIESBODY
		| linepair CIRCLECODES
		| xvalue CIRCLECODES
		| yvalue CIRCLECODES
		| radius CIRCLECODES
		;
			
LINECODES	: ENTITIESBODY
		| linepair LINECODES
		| xvalue LINECODES
		| yvalue LINECODES
		| xend LINECODES
		| yend LINECODES
		;
			
POLYLINECODES	: VERTEXS
		| linepair POLYLINECODES
		| xvalue POLYLINECODES
		| yvalue POLYLINECODES
		;
			
VERTEXS		: seqend { if (ver < verMin) verMin = ver; if (ver>verMax) {verMax = ver;} ver = 0;}  SEQENDCODES
		| vertex  { verc++; ver++; } VERTEXCODES
		;
			
SEQENDCODES	: ENTITIESBODY
		| linepair SEQENDCODES
		;
			
VERTEXCODES	: VERTEXS
		| linepair VERTEXCODES
		| xvalue VERTEXCODES
		| yvalue VERTEXCODES
		;
			
%%

int statistics_lex(YYSTYPE *dxf_value)
{
	return dxf_lex(dxf_value);
}


int yyerror(const char* s)
{
    	extern int dxf_lineno;
    	printf("%s @ line %d\n", s, dxf_lineno);
    	return 1;
}

void imprime()
{
	int media = 0, mediana = (verMax + verMin) / 2;

	if (polc != 0) media = verc / polc;

	printf("\nEntities existe!\n\n");
	printf("A entidade Circle apareceu %d vezes.\n",circ);
	printf("A entidade Line apareceu %d vezes.\n",linc);
	printf("A entidade Arc apareceu %d vezes.\n",arcc);
	printf("A entidade Polyline apareceu %d vezes.\n\n",polc);
	if(polc != 0)
	{
		printf("As Polylines tiveram um total de %d vértices.\n",verc);
		printf("A média de vértices por polyline foi %d, com mediana de %d.\n",media,mediana);
		printf("A entidade Polyline com maior número de vértices teve %d vértices.\n",verMax);
		printf("A entidade Polyline com menor número de vértices teve %d vértices.\n\n",verMin);
	}
}

void initializeCodes()
{
	arcc = 0;
	polc = 0;
	linc = 0;
	verc = 0;
	circ = 0;
	verMax = 0;
	verMin = 0;
	ver = 0;
	verMin = 0x7FFFFFFF;
}
