#include "Ponto.h"
#include <string.h>
#include <sstream>


int Ponto::pontCont = 0;
Ponto::Ponto(double a, double b): x(a), y(b){ flag = 0x3; pontOrder=pontCont++;}
Ponto::Ponto(){ flag = 0; pontOrder=pontCont++;}
Ponto::Ponto(const Ponto &p){
	x = p.x;
	y = p.y;
	flag = p.flag;
	pontOrder=p.pontOrder;
	
}
int Ponto::isValid(){ return flag == 0x3; }
void Ponto::setX(double x1) { x = x1; flag |= 1;}
void Ponto::setY(double y1){ y=y1; flag |= 2; }
double Ponto::getX() const{ return x;} 
double Ponto::getY() const{ return y;}
int Ponto::equals(Ponto a){
	return (getX() == a.getX() && getY() == a.getY()) || (getX() <= a.getX()+tolerance && getX() >= a.getX()-tolerance &&
						getY() <= a.getY()+tolerance && getY() >= a.y+tolerance);
		}
string Ponto::print(){ stringstream oss;
			oss << "Ponto_"<< pontOrder << '(' << x << ','<< y << ')';
			return oss.str(); }
bool Ponto::operator<(const Ponto& a) const{
	return (getX() < a.getX() || 	(getX() == a.getX() && getY() < a.getY()));

}
bool Ponto::operator==(const Ponto& a)const{
	return a.pontOrder == pontOrder ||(getX() == a.getX() && getY() == a.getY()) || (getX() <= a.getX()+tolerance && getX() >= a.getX()-tolerance && getY() <= a.getY()+tolerance && getY() >= a.y+tolerance);
}
