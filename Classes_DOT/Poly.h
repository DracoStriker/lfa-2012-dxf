#ifndef Poly_H
#define Poly_H

#include "Vertex.h"
#include "Entity.h"
#include <vector>
#include <iostream>
using namespace std;

class Poly: public Entity{
	public:
		int polyOrder;
		static int polyCont;
		vector<Vertex> vertices;
		int nVertex;
		Poly();
		void addVertex(Vertex x);
		int isValid();
		Vertex getNext() const;
		Vertex get(unsigned int) const;
		string print();
	
};

#endif
