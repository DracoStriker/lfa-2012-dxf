#include "Arco.h"
#include <cmath>
#include <sstream>

int Arco::arcCont = 0;
Arco::Arco(const Ponto& x, double w, double a, double b)
	:centro(x.getX(),x.getY())
	{
		raio=w;
		ang1=a;
		ang2=b;			
		flag = 0x0F;
		calcExtra();
		arcOrder= arcCont++;

	}
void Arco::calcExtra(){
	if(isValid()){
		extr1=Ponto(centro.getX()+(cos(ang1)*raio),centro.getY()+(cos(ang1)*raio));
		extr2=Ponto(centro.getX()+(cos(ang2)*raio),centro.getY()+(cos(ang2)*raio));
	}
}

Arco::Arco(){
	flag = 0;
	arcOrder= arcCont++;
}
void Arco::setCentro(Ponto centrum){
	if(centrum.isValid()){
		centro=centrum;
		flag |= center;
		calcExtra();
	}
	else flag = 0;
}
void Arco::setRaio (double r){
	raio = r;
	flag |= radius;
	calcExtra();
}
void Arco::setAng1 (double a){
	ang1 = a;
	flag |= angu1;
	calcExtra();
}
void Arco::setAng2 (double a){
	ang2 = a;
	flag |= angu2;
	calcExtra();
}
int Arco::isValid(){
	return flag==0x0F && centro.isValid() && extr1.isValid() && extr2.isValid();
}

Ponto Arco:: getCentro() const{return centro;} 
double Arco:: getRaio() const{return raio;}
double Arco:: getAng1() const{return ang1;}
double Arco:: getAng2() const{return ang2;}
Ponto Arco:: getExtr1() const{return extr1;}
Ponto Arco:: getExtr2() const{return extr2;}
string Arco::print(){ stringstream oss;
			oss << "Arco_"<< arcOrder /*<< "{ centro = " << centro.print() << ", raio = " << raio << ", " << extr1.print() << " , " << extr2.print() << " } "*/;
			return oss.str(); }
