#include "Vertex.h"
#include <sstream>


int Vertex::vertCont = 0;
Vertex::Vertex(Ponto a){ if(a.isValid()){ x = a; flag = 1;}else{ flag = 0; } vertOrder = vertCont++;};
Ponto Vertex::getX() const{ return x;} 
Vertex::Vertex(){ flag = 0; vertOrder = vertCont++;}
void Vertex::set(Ponto p){ if(p.isValid()){ x = p; flag = 1;}}
int Vertex::isValid(){ return flag && x.isValid(); }

string Vertex::print(){ stringstream oss;
			oss << "Vertex_"<< vertOrder<< "{ " << x.print() << " } ";
			return oss.str(); }
