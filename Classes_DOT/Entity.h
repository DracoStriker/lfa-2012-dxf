#ifndef Entit_H
#define Entit_H
#include <iostream>
using namespace std;
class Entity{
	public:
		int eOrder;
		Entity();
		virtual int isValid() = 0;
		static int entityCont;
		virtual string print() = 0;
};
#endif
