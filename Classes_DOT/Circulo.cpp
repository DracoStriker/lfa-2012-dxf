#include "Circulo.h"
#include <sstream>


int Circulo::circleCont = 0;
Circulo::Circulo(const Ponto &a): centro(a){ flag = 3;circOrder=circleCont++;};
Circulo::Circulo(){ flag = 0;circOrder=circleCont++;}
int Circulo::isValid(){ return flag == 3 && centro.isValid(); } 
void Circulo::setCentro(Ponto center){ if(center.isValid()) { centro=center; flag |= 1;}}
Ponto Circulo::getCentro() const{ return centro;} 
void Circulo::setRadius(double d){ raio = d; flag |= 2;} 
double Circulo::getRadius() const{ return raio; }
string Circulo::print(){ stringstream oss;
			oss << "Circle_"<< circOrder /*<< "{ centro = " << centro.print() << ", raio = " << raio << " } "*/;
			return oss.str();
}
