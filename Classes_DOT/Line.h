#ifndef Line_H
#define Line_H

#include "Ponto.h"
#include "Entity.h"
#include <iostream>
using namespace std;

class Line: public Entity{
	public:
		Ponto x;
		Ponto y;
		int flag;
		int linOrder;
		static int lineCont;
		Line(Ponto a, Ponto b);
		Line();
		void setExtr1(Ponto);
		void setExtr2(Ponto);
		int isValid();
		Ponto getExtr1() const;
		Ponto getExtr2() const;

		string print();
};
#endif
