	#include "Line.h"
#include <sstream>


int Line::lineCont = 0;
Line::Line(Ponto a, Ponto b): x(a), y(b) { flag = 0x3;linOrder=lineCont++;};
Line::Line(){ flag = 0;linOrder=lineCont++;}
void Line::setExtr1(Ponto p){ if(p.isValid()){ x = p; flag |= 1;}}
void Line::setExtr2(Ponto p){ if(p.isValid()){y = p; flag |= 2;}}
int Line::isValid(){ return flag == 0x3 && x.isValid() && y.isValid(); }
Ponto Line::getExtr1() const{ return x;} 
Ponto Line::getExtr2() const{ return y;}
string Line::print(){ stringstream oss;
			oss << "Line_"<< linOrder /*<< "{ " << x.print() << " , " << y.print() << " } "*/;
			return oss.str(); }
