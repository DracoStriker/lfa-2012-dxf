#ifndef PONTO_H
#define PONTO_H

#define tolerance 1e-5
#include <iostream>
using namespace std;
class Ponto{
	public:
		double x;
		double y;
		int flag;
		int pontOrder;
		static int pontCont;
	
		Ponto(double a, double b);
		Ponto();
		Ponto(const Ponto&);
		int isValid();
		void setX(double);
		void setY(double);
		double getX() const;
		double getY() const;
		int equals(Ponto a);	
		string print();
		bool operator<(const Ponto&) const;
		bool operator==(const Ponto& ) const;
};

struct Ponto_Compare {
  bool operator() (const Ponto& pt1, const Ponto& pt2) const
  {return pt1.x < pt2.x || (pt1.x == pt2.x && pt1.y < pt2.x);}
};
#endif
