#ifndef Circle_H
#define Circle_H

#include "Ponto.h"
#include "Entity.h"
#include <iostream>
using namespace std;

class Circulo: public Entity{
	public:		
		Ponto centro;
		double raio;
		int flag;
		static int circleCont;
		int circOrder;
	
		Circulo();
		Circulo(const Ponto &a);
		void setCentro(Ponto);
		void setRadius(double);
		Ponto getCentro() const;
		double getRadius() const;
		int isValid();
		string print();


};
#endif
