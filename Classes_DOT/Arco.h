#ifndef Arco_H
#define Arco_H

#include "Ponto.h"
#include "Entity.h"
#include <iostream>
using namespace std;

#define center 	(1<<0)
#define radius  (1<<1)
#define angu1	(1<<2)
#define angu2	(1<<3)
class Arco: public Entity{
	public:	
		Ponto centro;
		double raio;
		double ang1;
		double ang2;
		Ponto extr1;
		Ponto extr2;
		
		int arcOrder;
		static int arcCont;
	
		unsigned char flag; //controla se o Arco é válido (ou seja, se flag = 0x0F)
		// bit 0: centro definido
		// bit 1: raio definido
		// bit 2: ang 1 definido
		// bit 3: ang 2 definido
		void calcExtra();
	
		Arco(const Ponto& centro, double raio, double ang1, double ang2);
		Arco();
		void setCentro( Ponto);
		void setRaio (double);
		void setAng1 (double);
		void setAng2 (double);
		int isValid();
		Ponto getCentro() const;
		double getRaio() const;
		double getAng1() const;
		double getAng2() const;
		Ponto getExtr1() const;
		Ponto getExtr2() const;
		string print();
};
#endif
