#ifndef Vertex_H
#define Vertex_H

#include "Ponto.h"
#include "Entity.h"
#include <iostream>
using namespace std;

class Vertex: public Entity{
	public:	
		Ponto x;
		int flag;
		int vertOrder;
		static int vertCont;
	
		Vertex(Ponto a);
		Vertex();
		void set(Ponto);
		int isValid();
		Ponto getX() const;
		string print();
	
};
#endif
