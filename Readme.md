2nd Year Course - Formal Languages​ and Automata
-------------------------------------------

Project Members
---------------
Simão Reis 

Rafael Saraiva

Pedro Piçarra

Hugo Frade

Miguel Valério

Project Description
-------------------
Implementation of a parser and converter for the Autocad DXF format.
Output file formats:
* SVG
* HTML/SVG
* DOT

The user can also choose to print multiple statistics about the dxf input file.

Implementation
--------------
Flex/Bison tokenizer and parser;
C Programming Language.
