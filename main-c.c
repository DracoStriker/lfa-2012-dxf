/**
 * Linguagens Formais e Autómatos - 2011/12
 * 
 * DXF PARSER
 *
 * Bruno Marques - 47925
 * Pedro Piçarra - 59513
 * Rafael Figueiredo - 59863
 * Simão Reis - 59575
 * Tiago Novo - 60414
 *
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include <libgen.h>

static void printUsage(FILE* fout, const char* s)
{
    fprintf(fout, "Synopsis %s [options]\n"
    	"\t+----------+---------------------------------------------+\n"
    	"\t|  Option  |          Description                        |\n"
    	"\t+----------+---------------------------------------------+\n"
        "\t|   s      | avalia a correção sintática do ficheiro DXF |\n"
        "\t|   S      | imprime estatísticas sobre um ficheiro DXF  |\n"
        "\t|   X      | produz um ficheiro SVG                      |\n"
        "\t|   H      | produz um ficheiro HTML/SVG                 |\n"
        "\t|   D      | produz um grafo em DOT                      |\n"
        "\t|   i path | path para o ficheiro de entrada             |\n"
        "\t|   o path | path para o ficheiro de saída               |\n"
    	"\t|  -h      | mensagem de ajuda                           |\n"
    	"\t+----------+---------------------------------------------|\n"
        , s);
}

/************************************************************************************/

#define SYNTAX_ONLY (1<<0)
#define STATISTICS  (1<<1)
#define SVG         (1<<2)
#define HTML        (1<<3)
#define DOT         (1<<4)

int syntax_parse();
int statistics_parse();
int svg_parse();
int dot_parse();

extern FILE* dxf_in;
extern FILE* dxf_out;

int main (int argc, char *argv[])
{
	int error;
	
	/* init log stream */
	FILE *fin = stdin;
	FILE *fout = stdout;
    uint32_t mask = 0;

	/* processing command line */
	int op;
	while ((op=getopt(argc, argv, "sSXHDi:o:h")) != -1)
	{
		switch (op)
		{
            case 's': /* syntax-only requested */
            {
                if (mask != 0)
                {
                    fprintf(stderr, "Only one option allowed\n");
                    exit(EXIT_FAILURE);
                }
                mask = SYNTAX_ONLY;
                break;
            }

            case 'S': /* statistics requested */
            {
                if (mask != 0)
                {
                    fprintf(stderr, "Only one option allowed\n");
                    exit(EXIT_FAILURE);
                }
                mask = STATISTICS;
                break;
            }

            case 'X': /* svg output requested */
            {
                if (mask != 0)
                {
                    fprintf(stderr, "Only one option allowed\n");
                    exit(EXIT_FAILURE);
                }
                mask = SVG;
                break;
            }

            case 'H': /* html output requested */
            {
                if (mask != 0)
                {
                    fprintf(stderr, "Only one option allowed\n");
                    exit(EXIT_FAILURE);
                }
                mask = HTML;
                break;
            }

            case 'D': /* DOT graph requested */
            {
                if (mask != 0)
                {
                    fprintf(stderr, "Only one option allowed\n");
                    exit(EXIT_FAILURE);
                }
                mask = DOT;
                break;
            }

			case 'i': /* input file name */
            {
				if ((fin = fopen(optarg, "r")) == NULL)
				{
					fprintf(stderr, "Fail openning input file \"%s\"", optarg);
					exit(EXIT_FAILURE);
				}
				break;
            }

			case 'o': /* output file name */
            {
				if ((fout = fopen(optarg, "w")) == NULL)
				{
					fprintf(stderr, "Fail openning output file \"%s\"", optarg);
					exit(EXIT_FAILURE);
				}
				break;
            }

			case 'h': /* print help and quit */
            {
				printUsage(stdout, basename(argv[0]));
				exit(EXIT_SUCCESS);
            }

			default:
            {
				printUsage(stderr, basename(argv[0]));
				exit(EXIT_FAILURE);
            }
		}
	}
	
	switch(mask)
    {
		case SYNTAX_ONLY:
			dxf_in = fin;
			if ((error = syntax_parse()) == 0) { printf("syntax OK\n"); }
			return error;
				
		case STATISTICS:
			dxf_in = fin;
			if ((error = syntax_parse()) != 0) return error;
			rewind(dxf_in);
			return statistics_parse();
	
		case SVG:
			dxf_in = fin;
			if ((error = syntax_parse()) != 0) return error;
			rewind(dxf_in);
			dxf_out = fout;
			return svg_parse();
			
		case HTML:
			dxf_in = fin;
			if ((error = syntax_parse()) != 0) return error;
			rewind(dxf_in);
			dxf_out = fout;
			fprintf(dxf_out,"<html>\n<body>\n");
			error = svg_parse();
			fprintf(dxf_out,"</body>\n</html>");
			return error;
			
		case DOT:
			dxf_in = fin;
			if ((error = syntax_parse()) != 0) return error;
			rewind(dxf_in);
			dxf_out = fout;
			return dot_parse();
	}
	return 0;
}
